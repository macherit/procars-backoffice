const path = require("path");

module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "/admin/" : "/",
  pages: {
    index: {
      entry: "src/site/main.js",
      template: "public/index.html",
      filename: "index.html"
    }
  },
  devServer: {
    historyApiFallback: {
      rewrites: [
        {
          from: {},
          to: "/index.html"
        }
      ]
    },
    proxy: {
      "/api": {
        target: "http://localhost:3030/",
        changeOrigin: true,
        pathRewrite: {
          "^/api": ""
        }
      },
      "/public": {
        target: "http://localhost:3000/"
      }
    }
  },
  chainWebpack: config => {
    config.resolve.alias.set("@", path.resolve(__dirname, "src/"));
    config.resolve.alias.set("Styles", path.resolve(__dirname, "src/assets/scss"));
  },
  transpileDependencies: ["vuetify"]
};
