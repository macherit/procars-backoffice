export default {
  install(Vue) {
    Vue.mixin({
      methods: {
        async uploadedFile(file) {
          if (file) {
            try {
              const data = new FormData();
              data.append("uri", file);

              const {
                data: { id: uri }
              } = await this.$http({
                method: "POST",
                url: `${this.SERVER_URL}/uploads`,
                headers: {
                  "Content-Type": "multipart/form-data"
                },
                body: data
              });

              return uri;
            } catch (error) {
              this.errorMessage = error.body || error.bodyText || error;
              // console.error(this.errorMessage);
            }
          }
          return null;
        }
      }
    });
  }
};
