import axios from "axios";

const SERVER_URL = "/api";

export default {
  login(email, password, callback) {
    axios({
      method: "POST",
      url: `${SERVER_URL}/authentication`,
      data: { email, password, strategy: "local" }
    }).then(
      res => {
        if (res.status === 201) {
          localStorage.setItem("@procargBackoffice:token", res.data.accessToken);

          localStorage.setItem(
            "@procargBackoffice:user",
            JSON.stringify({
              id: res.data.user.id,
              email: res.data.user.email
            })
          );
        }
        callback(null, res);
      },
      error => {
        callback(error, null);
      }
    );
  },
  resetTK(tk) {
    localStorage.setItem("@procargBackoffice:token", tk);
  },
  logout() {
    localStorage.removeItem("@procargBackoffice:token");
    localStorage.removeItem("@procargBackoffice:user");
  }
};
