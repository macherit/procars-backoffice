// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import VueResource from "vue-resource";
import VeeValidate from "vee-validate";
import VueLodash from "vue-lodash";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faCoffee } from "@fortawesome/free-solid-svg-icons";
import faSolid from "@fortawesome/fontawesome-free-solid";
import faBrands from "@fortawesome/fontawesome-free-brands";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import FormPlugin from "@/site/plugins/FormsPlugin";
import GlobalVariables from "@/site/plugins/GlobalVariables";
import FilesPlugin from "@/site/plugins/FilesPlugin";
import App from "@/site/App";
import router from "@/site/router";
import store from "@/site/store/index";
import vuetify from "@/site/plugins/vuetify";

library.add(faCoffee, faSolid, faBrands);

Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.use(VueResource);
Vue.use(VeeValidate);
Vue.use(VueLodash);
Vue.use(FormPlugin);
Vue.use(GlobalVariables);
Vue.use(FilesPlugin);

Vue.http.headers.common.Authorization = localStorage.getItem("@procargBackoffice:token")
  ? localStorage.getItem("@procargBackoffice:token")
  : "";

Vue.config.productionTip = false;

/* eslint-disable no-new */
window.App = new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
