import userService from "@/site/services/users";

const hasTokenExpired = token => {
  const outObj = { loggedIn: true, token };

  if (new Date(JSON.parse(window.atob(token.split(".")[1])).exp * 1000) < new Date()) {
    // Token outdated
    outObj.loggedIn = false;
    outObj.token = null;

    userService.logout();
  }

  return outObj;
};

export default {
  namespaced: true,
  state: localStorage.getItem("@procargBackoffice:token")
    ? hasTokenExpired(localStorage.getItem("@procargBackoffice:token"))
    : { loggedIn: false, token: null },
  getters: {
    user(state) {
      let outObj = false;
      if (state.loggedIn) {
        const user = JSON.parse(localStorage.getItem("@procargBackoffice:user"));

        outObj = {
          ...user
        };
      }
      return outObj;
    }
  },
  actions: {
    login({ commit }, { email, password, callback }) {
      userService.login(email, password, (error, res) => {
        if (error) {
          callback(error, null);
        } else {
          commit("loginSuccess", res.data.accessToken);
          callback(null, res);
        }
      });
    },
    logout({ commit }) {
      userService.logout();
      commit("logout");
    }
  },
  mutations: {
    loginSuccess(state, token) {
      state.loggedIn = true;
      state.token = token;
    },
    loginFailure(state) {
      state.loggedIn = false;
      state.token = null;
    },
    logout(state) {
      state.loggedIn = false;
      state.token = null;
    }
  }
};
