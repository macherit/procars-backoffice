const Delete = () =>
  import(/* webpackChunkName: "delete-clientes" */ "@/site/views/Clientes/Delete");
const New = () => import(/* webpackChunkName: "new-clientes" */ "@/site/views/Clientes/New");
const Edit = () => import(/* webpackChunkName: "edit-clientes" */ "@/site/views/Clientes/Edit");
const Index = () => import(/* webpackChunkName: "index-clientes" */ "@/site/views/Clientes/Index");

export default function() {
  return [
    {
      path: "/clientes/new",
      component: New,
      meta: {
        requiresLogin: true
      }
    },
    {
      path: "/clientes/:id/delete",
      component: Delete,
      meta: {
        requiresLogin: true
      }
    },
    {
      path: "/clientes/:id/edit",
      component: Edit,
      meta: {
        requiresLogin: true
      }
    },
    {
      path: "/clientes",
      component: Index,
      meta: {
        requiresLogin: true
      }
    }
  ];
}
