const Login = () => import(/* webpackChunkName: "login-users" */ "@/site/views/Users/Login");

export default function() {
  return [
    {
      path: "/login",
      component: Login,
      meta: {
        requiresLoggedOut: true
      }
    }
  ];
}
