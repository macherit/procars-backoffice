import Vue from "vue";
import Router from "vue-router";
import Home from "@/site/views/Home";

import store from "@/site/store/index";

import users from "./users";
import marcas from "./marcas";
import clientes from "./clientes";
import vehiculos from "./vehiculos";

Vue.use(Router);

const router = new Router({
  // mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home,
      meta: {
        requiresLogin: true
      }
    },

    ...users(),
    ...marcas(),
    ...clientes(),
    ...vehiculos(),

    { path: "*", redirect: "/" }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresLogin)) {
    if (!store.state.users.loggedIn) {
      next({
        path: "/login"
      });
    } else {
      next();
    }
  } else if (to.matched.some(record => record.meta.requiresLoggedOut)) {
    if (store.state.users.loggedIn) {
      next({
        path: "/"
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
