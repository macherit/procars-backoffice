const Delete = () =>
  import(/* webpackChunkName: "delete-vehiculos" */ "@/site/views/Vehiculos/Delete");
const New = () => import(/* webpackChunkName: "new-vehiculos" */ "@/site/views/Vehiculos/New");
const Edit = () => import(/* webpackChunkName: "edit-vehiculos" */ "@/site/views/Vehiculos/Edit");
const Index = () =>
  import(/* webpackChunkName: "index-vehiculos" */ "@/site/views/Vehiculos/Index");

export default function() {
  return [
    {
      path: "/vehiculos/new",
      component: New,
      meta: {
        requiresLogin: true
      }
    },
    {
      path: "/vehiculos/:id/delete",
      component: Delete,
      meta: {
        requiresLogin: true
      }
    },
    {
      path: "/vehiculos/:id/edit",
      component: Edit,
      meta: {
        requiresLogin: true
      }
    },
    {
      path: "/vehiculos",
      component: Index,
      meta: {
        requiresLogin: true
      }
    }
  ];
}
