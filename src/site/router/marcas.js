const Delete = () => import(/* webpackChunkName: "delete-marcas" */ "@/site/views/Marcas/Delete");
const New = () => import(/* webpackChunkName: "new-marcas" */ "@/site/views/Marcas/New");
const Edit = () => import(/* webpackChunkName: "edit-marcas" */ "@/site/views/Marcas/Edit");
const Index = () => import(/* webpackChunkName: "index-marcas" */ "@/site/views/Marcas/Index");

export default function() {
  return [
    {
      path: "/marcas/new",
      component: New,
      meta: {
        requiresLogin: true
      }
    },
    {
      path: "/marcas/:id/delete",
      component: Delete,
      meta: {
        requiresLogin: true
      }
    },
    {
      path: "/marcas/:id/edit",
      component: Edit,
      meta: {
        requiresLogin: true
      }
    },
    {
      path: "/marcas",
      component: Index,
      meta: {
        requiresLogin: true
      }
    }
  ];
}
