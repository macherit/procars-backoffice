#!/bin/bash

yarn run build

cd dist

if test -f dist.tar; then
  rm dist.tar
fi

tar -cvf dist.tar *

scp -P 7822 dist.tar procarg@75.98.172.211:/home/procarg/public_html/admin/

rm dist.tar

ssh procarg@75.98.172.211 -p 7822 'cd /home/procarg/public_html/admin/; tar -xvf dist.tar; rm dist.tar'